# Dynamic Table View Cells

Quick and dirty example of changing the height of a dynamic table view cell.

## What it does.

Run the app and a table view of images and labels appears. Each cell has a switch which toggles the image on and off.
