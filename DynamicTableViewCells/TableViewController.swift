//
//  TableViewController.swift
//  DynamicTableViewCells
//
//  Created by Abizer Nasir on 24/08/2016.
//  Copyright © 2016 Jungle Candy Software Ltd. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {

    var datasource: [Image]!

    override func viewDidLoad() {
        super.viewDidLoad()
        generateDataSource()
        tableView.estimatedRowHeight = 125
        tableView.rowHeight = UITableViewAutomaticDimension
    }

    // MARK: - Table view data source

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("TableViewCell", forIndexPath: indexPath) as! TableViewCell

        let image = datasource[indexPath.row]
        cell.configure(image, switchHandler: switchHandler())

        return cell
    }
}

private extension TableViewController {

    func generateDataSource() {
        datasource = [
            Image(imageName: "cats", title: "First", showImage: true),
            Image(imageName: "cats", title: "Second", showImage: true),
            Image(imageName: "cats", title: "Third", showImage: true),
            Image(imageName: "cats", title: "Fourth", showImage: true),
            Image(imageName: "cats", title: "Fifth", showImage: true),
            Image(imageName: "cats", title: "Sixth", showImage: true),
            Image(imageName: "cats", title: "Seventh", showImage: true),
            Image(imageName: "cats", title: "Eighth", showImage: true),
            Image(imageName: "cats", title: "Ninth", showImage: true),
            Image(imageName: "cats", title: "Tenth", showImage: true)
        ]
    }

    func switchHandler() -> (UITableViewCell) -> Void {
        return { [weak self] cell -> Void in
            guard
                let strongSelf = self,
                let indexPath = strongSelf.tableView.indexPathForCell(cell),
                let tableView = strongSelf.tableView
                else { return }
            var images = strongSelf.datasource
            let index = indexPath.row
            var image = images[index]
            image.showImage = !image.showImage
            let range = Range<Int>(index ..< index+1)
            images.replaceRange(range, with: [image])
            strongSelf.datasource = images

            tableView.beginUpdates()
            tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
            tableView.endUpdates()
        }
    }
}
