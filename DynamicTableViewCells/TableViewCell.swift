//
//  TableViewCell.swift
//  DynamicTableViewCells
//
//  Created by Abizer Nasir on 24/08/2016.
//  Copyright © 2016 Jungle Candy Software Ltd. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet var cellImageView: UIImageView!
    @IBOutlet var label: UILabel!
    @IBOutlet var toggle: UISwitch!
    @IBOutlet var imageViewHeightConstraint: NSLayoutConstraint!

    var switchHandler: ((UITableViewCell) -> Void)?

    func configure(image: Image, switchHandler: (UITableViewCell) -> Void) {
        self.switchHandler = switchHandler
        label.text = image.title

        if image.showImage {
            imageViewHeightConstraint.constant = 80
            cellImageView.image = image.image
        } else {
            imageViewHeightConstraint.constant = 0
            cellImageView.image = nil
        }

        toggle.setOn(image.showImage, animated: false)
    }

    @IBAction func toggleImage(sender: UISwitch) {
        switchHandler?(self)
    }


}
