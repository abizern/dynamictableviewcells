//
//  Image.swift
//  DynamicTableViewCells
//
//  Created by Abizer Nasir on 24/08/2016.
//  Copyright © 2016 Jungle Candy Software Ltd. All rights reserved.
//

import UIKit

/// The model object for the application
struct Image: Equatable {

    let imageName: String
    let title: String
    var showImage = true

    var image: UIImage {
        return UIImage(named: imageName)! // Force unwrapping as this is an example. You don't want to do this with production code.
    }
}

func == (lhs: Image, rhs: Image) -> Bool {
    return lhs.imageName == rhs.imageName && lhs.title == rhs.title
}
